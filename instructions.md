# Instructions

Let's set up the project to use grunt.

## CSS

The flow for generating stylesheets is the following:

  * Compile SASS project from the "src/sass" directory to the "src/temp/css/" directory using [grunt-contrib-sass](https://github.com/gruntjs/grunt-contrib-sass).
  * Add vendor prefixes and minify all CSS files in "src/temp/css" into the "public/styles" directory using [grunt-postcss](https://github.com/nDmitry/grunt-postcss).

Configure your gruntfile to perform these actions, then create a custom task "styles" that will perform those tasks in the correct order.

## JS

There are two categories of JS files with different flows:

  * the Node server
  * the client-side scripts

### Node Server

The flow for the node server is the following:

  * Lint ".js" and ".json" files in the root directory and the "lib" directory using [grunt-contrib-jshint](https://github.com/gruntjs/grunt-contrib-jshint).
  * Beautify those files using [grunt-jsbeautifier](https://github.com/vkadam/grunt-jsbeautifier).

Configure your gruntfile to perform these actions, then create a custom task "node" that will perform those tasks in the correct order.

### Client Script Files

The flow for the client script files is the following:

  * Lint ".js" files in the "src/js" directory using [grunt-contrib-jshint](https://github.com/gruntjs/grunt-contrib-jshint).
  * Beautify those files using [grunt-jsbeautifier](https://github.com/vkadam/grunt-jsbeautifier).
  * Transpile to es5 all js files in "src/js" directory to the "src/temp/js/es5" directory using [grunt-babel](https://github.com/babel/grunt-babel).
  * Concatenate all es5 files in "src/temp/js/es5" to "src/temp/js/concat" using [grunt-contrib-concat](https://github.com/gruntjs/grunt-contrib-concat).
  * Minify the concatenated js file into the "public/scripts" directory using [grunt-contrib-uglify](https://github.com/gruntjs/grunt-contrib-uglify).

  Configure your gruntfile to perform these actions, then create a custom task "scripts" that will perform those tasks in the correct order.

## Watch File Changes

Watch file changes to kick off their custom tasks using [grunt-contrib-watch](https://github.com/gruntjs/grunt-contrib-watch):

  * Watch SASS files to kick off the "styles" task.
  * Watch Node server files to kick off the "node" task.
  * Watch script files to kick off the "scripts" task.

Additionally, you can set up :

  * [grunt-contrib-watch](https://github.com/gruntjs/grunt-contrib-watch#live-reloading) to live reload your page when grunt detects a change.
  * [nodemon](https://github.com/remy/nodemon) to live reload your server when node detects a change.

## Optimize Gruntfile

Try cleaning up your gruntfile with these plugins:

  * Simplify loading tasks with [load-grunt-tasks](https://github.com/sindresorhus/load-grunt-tasks).
  * Break out configs into their own files with [load-grunt-config](https://www.npmjs.com/package/load-grunt-config).
